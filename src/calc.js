'use strict';

var calc = {
  add: (a, b) => {
    return a + b;
  },
};

export { calc };
