## Introduction

* Node.js <--> io.js
* JavaScript <--> ES2015
* Gulp

## build system

* gulp

## unit testing

* Mocha: testing framework
* Chai: TDD/BDD assertion library
    * Should
        * `foo.should.be.a('string');`
    * Expect
        * `expect(foo).to.be.a('string');`
    * Assert
        * `assert.typeOf(foo, 'string');`

## E2E testing

* webdriver.io

## code coverage

* istanbul

## coding style

* JSCS
* JSHint
