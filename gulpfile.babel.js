'use strict';

import babel from 'gulp-babel';
import chai from 'chai';
import concat from 'gulp-concat';
import del from 'del';
import gulp from 'gulp';
import importCss from 'gulp-import-css';
import istanbul from 'gulp-istanbul';
import jscs from 'gulp-jscs';
import jshint from 'gulp-jshint';
import minifyCss from 'gulp-minify-css';
import mocha from 'gulp-mocha';
import rename from 'gulp-rename';
import stylish from 'jshint-stylish';
import uglify from 'gulp-uglify';
import webdriver from 'gulp-webdriver';

gulp.task('build:script', () => {
  gulp.src('src/index.js')
    .pipe(babel())
    .pipe(uglify())
    .pipe(rename('app.js'))
    .pipe(gulp.dest('dist'));
  gulp.src(['dist/app.js', 'node_modules/material-design-lite/material.min.js'])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('dist'));
});

gulp.task('build:style', () => {
  return gulp.src('src/index.css')
    .pipe(importCss())
    .pipe(minifyCss())
    .pipe(rename('styles.css'))
    .pipe(gulp.dest('dist'));
});

gulp.task('build:html', () => {
  return gulp.src('src/**/*.html')
    .pipe(gulp.dest('dist'));
});

gulp.task('build', ['build:script', 'build:style', 'build:html'], () => {
  return gulp.src('src/favicon.ico')
    .pipe(gulp.dest('dist'));
});

// Clean
gulp.task('clean', cb => {
  del([
    'dist/*',
    'coverage/*',
  ], cb);
});

// JSCS + JSHint
gulp.task('lint', () => {
  return gulp.src([
      'src/**/*.js',
      'test/**/*.js',
      'gulpfile.babel.js',
    ])
    .pipe(jscs())
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});

// E2E testing
gulp.task('e2e', () => {
  global.baseUrl = 'https://localhost:3000';
  return gulp.src('test/e2e/*.spec.js', {
    read: false,
  })
  .pipe(webdriver({
    desiredCapabilities: {
      browserName: 'chrome',
    },
  }));
});

// Unit testing
gulp.task('test', cb => {
  global.expect = chai.expect;
  gulp.src(['src/**/*.js'])
    .pipe(babel())
    .pipe(istanbul())
    .pipe(istanbul.hookRequire())
    .on('finish', () => {
      gulp.src(['test/*.spec.js'])
        .pipe(mocha())
        .pipe(istanbul.writeReports())
        .pipe(istanbul.enforceThresholds({ thresholds: { global: 90 } }))
        .on('end', cb);
    });
});
