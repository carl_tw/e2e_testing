# e2e_testing

## Prerequisites

* JDK 8
* io.js / Node.js
* selenium-standalone
* chromedriver

## Unit testing

* `gulp test`

## E2E testing
* `gulp e2e`
