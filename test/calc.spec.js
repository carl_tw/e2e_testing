'use strict';

import { calc } from '../src/calc';

describe('Calculator', () => {
  describe('#add', () => {
    it('should return the sum of both operands', () => {
      expect(calc.add(2, 2)).to.equal(4);
    });
  });
});
