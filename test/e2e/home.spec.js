'use strict';

describe('首頁', () => {
  beforeEach(function*() {
    yield browser.url(baseUrl + '/');
  });

  it('網站標題正確', function*() {
    let title = yield browser.getTitle();
    expect(title).to.equal('Gogs: Go Git Service');
  });

  it('內文標題正確', function*() {
    let heading = yield browser.getValue('.hero.title');
    expect(heading).to.equal('Gogs - Go Git Service');
  });

  it('網站可以註冊', function*() {
    let register = yield browser.getValue('.octicon-person-add');
    expect(register).to.equal('Register');
  });

  it('網站可以登入', function*() {
    let signin = yield browser.getValue('.octicon-sign-in');
    expect(signin).to.equal('Sign In');
  });
});
