'use strict';

describe('註冊', () => {
  beforeEach(function*() {
    yield browser.url(baseUrl + '/user/sign_up');
  });

  it('表單標題正確', function*() {
    let title = yield browser.getValue('.panel-header');
    expect(title).to.equal('Sign Up');
  });

  it('有使用者名稱欄位', function*() {
    let isVisible = yield browser.isVisible('#username');
    expect(isVisible).to.be.true;
  });

  it('有電子信箱欄位', function*() {
    let isVisible = yield browser.isVisible('#email');
    expect(isVisible).to.be.true;
  });

  it('有設定密碼欄位', function*() {
    let isVisible = yield browser.isVisible('#password');
    expect(isVisible).to.be.true;
  });

  it('有確認密碼欄位', function*() {
    let isVisible = yield browser.isVisible('#re-type');
    expect(isVisible).to.be.true;
  });

  it('有驗證碼欄位', function*() {
    let isVisible = yield browser.isVisible('#captcha');
    expect(isVisible).to.be.true;
  });

  it('有新增帳號按鈕', function*() {
    let button = yield browser.setValue('.form-label.btn');
    expect(button).to.equal('Create New Account');
  });
});
