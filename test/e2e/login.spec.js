'use strict';

describe('登入', () => {
  beforeEach(function*() {
    yield browser.url(baseUrl + '/user/login');
  });

  it('帳號密碼正確', function*() {
    yield browser
      .setValue('#username', 'carlsu')
      .setValue('#password', '5iveL!fe')
      .click('.form-label.btn');
    let navbarCurrent = yield browser.getValue('#header-nav.current a');
    expect(navbarCurrent).to.equal('Dashboard');
  });
});
